**TP1 Systemd**

**FIRST STEP**

Vérifier que la version de systemd est >241 : Pour ce faire j'utilise la commande "systemctl --version " et on voit donc que le version est a 243



S’assurer que systemd est PID1 : Grace a la commande ps -e on voit bien que la première ligne correspond a systemd avec un PID de 1



Liste de 5 autres processus : 

    -crontab qui permet de planifier des tâches
    -sshd service de d’authentification à distance de manière assez sécurisée
    -NetworkManager est l’outils de gestion des connexions réseaux
    -dockerd est un logiciel libre à mi-chemin entre la virtualisation applicative et l'automatisation. Il permet de manipuler des conteneurs de logiciels.
    -rsyslogd permet de collecter les messages de service provenant des applications et du noyau puis de les répartir dans des fichiers de logs



**2.Gestion du temps**

Horloge : 

    -Local time = horloge du pays ou l’on se situe (FRANCE)
    -Universal time = heure internationale
    -RTC Time = horloge du bios

Le RTC Time permet d’automatiser certaines tâches à une heure précise 


Changer de timezone pour un autre fuseau horaire européen : timedatectl set-timezone Europe/Vilnius


Désactiver le service lié à la synchronisation du temps avec cette commande, et vérifier à la main qu'il a été coupé : Il faut d'abord effectuer 
timedatectl set-timezone, ensuite grace a la commande timedatectl show on peut voir que la valeur de NTP correspond a NO



**3.Gestion de noms**



Changer les noms de la machine avec hostnamectl set-hostname : hostnamectl set-hostname theo




Hostnamectl :

    -Static = méthode la plus utilisé, peut être changé par l’utilisateur
    -Transient = méthode maintenu par le noyau kernel, peut être changé avec le DNS et le DHCP
    -Pretty = 


Pour des machines en machine de prod, le --transient est le plus intéressant


**4.Gestion du réseau (et résolution de noms)**

nmcli con show liste les différentes cartes présentes

    -enp0s3
    -docker0



**systemd-networkd**

Stopper et désactiver le démarrage de NetworkManager : les 2 commandes a effectuées sont

                                                    -systemctl stop NetworkManager
                                                    -systemctl disable NetworkManager




Démarrer et activer le démarrage de systemd-networkd : les 2 commandes a effectuées sont

                                                    -systemctl start systemd-networkd
                                                    -systemctl enable systemd-networkd



Editer la configuration d'une carte réseau de la VM avec un fichier .network 

"Extrait de mon fichier .network"

[Match]
Name=enp0s3

[Network]
Address=192.168.10.17/24
DNS=1.1.1.1





**systemd-resolved**

Activer le systemd-resolved au démarrage : systemctl enable systemd-resolved





Vérification qu’un service tourne bien grâce à la commande ss : grace a la commande ss -l | 127.0
j'observe qu'un service udp apparaît et qu'un service tcp est en ecoute



Faire un lien symbolique :

ln -sf /run/systemd/resolve/stub-resolv.conf /etc/resolv.conf
Ajout du serveur 8.8.8.8 dans le systemd-resolved :
"Je n'ai pas réussi a déposer la preuve en image..."





trouver l’unité associée au processus Chronyd : je trouve l'unité grâce a la commande ps -e -o pid,cmd,unit | chronyd




**II. Boot et Logs**

déterminer le temps qu'a mis sshd.service à démarrer : grace a la commande cat graphe.svg | grep ssh j'ai vu que le service ssh a mis 651 ms à démarrer


**III. Mécanismes manipulés par systemd**

**1.cgroups**



Le cgroup de la session ssh est obtenu grâce a la commande : ps -e -o pid,cmd,unit | chronyd.



La commande pour changer la mémoire max d’un service, ici SSH est : systemctl set-property sshd.service MemoryMax=512M



Puis suite à la modification précédente, on voit bien que le fichier 50-MemoryMax.conf est créé dans le service (ssh) souhaité.

**2.D-Bus**
